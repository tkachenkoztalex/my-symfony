<?php
// src/AppBundle/Repository/CategoryRepository.php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;



/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Category::class);
    }


    /**
     * @param RegistryInterface $registry
     * @return mixed
     * @method Category[]    findAll()
     */
    public function subCategory(RegistryInterface $registry)
    {
        $category = $this->getDoctrine()->getRepository(category::class)->find('parent_id');

        return $category;
    }



    /**
     * @param RegistryInterface $registry
     * @return mixed
     * @method Category[]
     */
    public function getSubCategories(RegistryInterface $registry)
    {
        $className = Category::class;
        $categories = $this->getDoctrine()->getRepository($className);

        $subCategories = [];
        foreach ($categories->findAll() as $category)
        {
            if(!isset($subCategories[$category->getParentId()]) || !is_array($subCategories[$category->getParentId()]))
            {
                $subCategories[$category->getParentId()] = [];
            }
            array_push($subCategories[$category->getParentId()], $category);
        }

        return $subCategories;
    }









//    /**
//     * @return Category[] Returns an array of Category objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Category
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
