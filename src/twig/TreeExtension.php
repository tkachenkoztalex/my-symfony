<?php

namespace App\twig;

use App\Entity\Category;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class TreeExtension extends \Twig_Extension
{

    public function getFunctions()
    {
        $function = function ($tree) {
            $result = $this->build_tree($tree);
            return $result;
        };
        return array(
            new \Twig_SimpleFunction('tree', $function),
        );
    }


    /**
     * Строит дерево категорий
     * @param array $cats
     * @return string
     */
    public function build_tree(array $cats)
    {
        $tree = '<ul>';
        foreach ($cats as $cat) {
            $tree .= '<li>' . $cat->title();
            if ($cat->getParentId() != null) {
                $childs = $cat->getParentId()->toArray();
                $tree .= $this->build_tree($childs);
            }
            $tree .= '</li>';
        }
        $tree .= '</ul>';
        return $tree;
    }


}