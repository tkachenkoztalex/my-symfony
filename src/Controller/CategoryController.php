<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;


class CategoryController extends Controller
{
    /**
     * @Route("/category", name="category")
     */
    public function index()
    {
        $date = $this->getDoctrine()->getRepository(category::class)->findAll();

        return $this->render('category/panel.html.twig', [
            'date' => $date,
        ]);
    }


    public function createdAction(Request $request)
    {
        $className = Category::class;
        $repo = $this->getDoctrine()->getRepository($className);

        $category = new $className();
        $category->setCreatedAt(new \DateTime());
        $category->setUpdatedAt(new \DateTime());

        $choices = [];
        foreach ($repo->findAll() as $cat) {
            $choices[$cat->getTitle()] = $cat->getId();
        }

        $form = $this->createFormBuilder($category)
            ->add('title', TextType::class, array('label' => 'Название Категории'))
            ->add('parentId', ChoiceType::class, array(
                'choices' => $choices,
                'required' => false,
            ))
            ->add('createdAt', DateTimeType::class, array('label' => 'Дата Создания'))
            ->add('updatedAt', DateTimeType::class, array('label' => 'Дата Редактирования'))
            ->add('save', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $category = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();
            return $this->redirectToRoute('category_panel');
        }

        return $this->render('category/categoryCreated.html.twig', array(
            'form' => $form->createView()
        ));
    }


    /**
     * @Route("/category/edit/{id}")
     */
    public function updateAction($id, Request $request)
    {
        $em = $this->getDoctrine();
        $cat = $em->getRepository(category::class)->find($id);
        if (!$cat) {
            echo('404');
        }
        $form = $this->createFormBuilder($cat)
            ->add('title', TextType::class, array('label' => 'Название Категории'))
            ->add('parent_id', ChoiceType::class, array(
                'choices' => array(
                    'Категория' => 1,
                    'SUB1Категория' => 1,
                    'SUB2егория' => 1,
                    'SUB3егория' => 1,
                ),))
            ->add('created_at', DateTimeType::class, array('label' => 'Дата Создания'))
            ->add('updated_at', DateTimeType::class, array('label' => 'Дата Редактирования'))
            ->add('save', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $category = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();
            return $this->redirectToRoute('category_panel');
        }

        return $this->render('category/categoryCreated.html.twig', array(
            'form' => $form->createView()
        ));

    }


    /**
     * @Route("/category/dest/{id}")
     */
    public function destroy($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $category = $entityManager->getRepository(Category::class)->find($id);
        $entityManager->remove($category);
        $entityManager->flush();
        return $this->redirectToRoute('category_panel');
    }
}