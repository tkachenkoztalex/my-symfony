<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Repository\CategoryRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;

class HomeController extends Controller
{
        /**
         * @Route("/home", name="home")
         */
        public function index(CategoryRepository $CategoryRepository)
        {
            // article output
            $em = $this->getDoctrine();
            $date = $em->getRepository(article::class)->findAll();

            // tree category output
            $className = Category::class;
            $categories = $this->getDoctrine()->getRepository($className);

            $subCategories = [];
            foreach ($categories->findAll() as $category) {
                if (!isset($subCategories[$category->getParentId()]) || !is_array($subCategories[$category->getParentId()])) {
                    $subCategories[$category->getParentId()] = [];
                }
                array_push($subCategories[$category->getParentId()], $category);
            }


            return $this->render('home/index.html.twig', [
                'controller_name' => 'HomeController',
                'category'=> $categories->findAll(),
                'date'=>$date,
                'subCategories'=>$subCategories,
            ]);
        }



}
