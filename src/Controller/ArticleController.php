<?php

namespace App\Controller;


use App\Entity\Article;
use App\Entity\Category;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;



class ArticleController extends Controller
{
    /**
     * @Route("/article", name="article")
     */
    public function index(Request $request){

        $em = $this->getDoctrine();
        $date = $em->getRepository(article::class)->findAll();
        

        return $this->render('article/articlePanel.html.twig', [
            'date' => $date,
        ]);

    }


    /**
     * @Route("category/created")
     *
     */
    public function created(Request $request)
    {
        $article = new article();
        $article->setCreatedAt(new \DateTime());
        $article->setUpdatedAt(new \DateTime());

        $date = $this->getDoctrine()->getRepository(category::class)->findAll();

        $form = $this->createForm( ArticleType::class,$article);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $article = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();
            return $this->redirectToRoute('article_panel');
        }

        return $this->render('article/createArticle.html.twig', array(
            'form' => $form->createView()
        ));
    }



    /**
     * @Route("/category/edit/{id}")
     */
    public function updateAction($id , Request $request)
    {
        $em = $this->getDoctrine();


        $article = $em->getRepository(article::class)->find($id);
        if(!$article){
            echo ('404');
        }

        $form = $this->createForm( ArticleType::class,$article);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $article = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();
            return $this->redirectToRoute('article_panel');
        }

        return $this->render('article/createArticle.html.twig', array(
            'form' => $form->createView()
        ));
    }


    /**
     * @Route("/category/destroy/{id}")
     */
    public function destroy($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $article = $entityManager->getRepository(article::class)->find($id);
        $entityManager->remove($article);
        $entityManager->flush();
        return $this->redirectToRoute('article_panel');
    }
}
