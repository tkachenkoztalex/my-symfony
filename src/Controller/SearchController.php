<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SearchController extends Controller
{
    /**
     * @Route("/search", name="search")
     * @param Request $request
     * @param ArticleRepository $repo
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
            $em = $this->getDoctrine()->getManager();

            $requestString = $request->get('q');

            $entities = $em->getRepository('App:Article')->findEntitiesByString($requestString);

        if(!$entities) {
            $result['entities']['error'] = "keine Einträge gefunden";
        } else {
            $result['entities'] = $this->getRealEntities($entities);
        }
        return new Response(json_encode($result));
    }


    public function getRealEntities($entities){
        foreach ($entities as $entity){
            //$realEntities[$entity->getId()] = $entity->getTitle();
            $realEntities[$entity->getId()]['title']= $entity->getTitle();
            $realEntities[$entity->getId()]['content']= $entity->getContent();
        }
        return $realEntities;
    }


    public function created()
    {
        $em = $this->getDoctrine();
        $date = $em->getRepository(article::class)->findAll();

        return $this->render('search/index.html.twig', array(
            'date'=>$date
        ));
    }

}
