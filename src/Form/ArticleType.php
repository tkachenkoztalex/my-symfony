<?php

namespace App\Form;

use App\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class ArticleType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('title', TextType::class, array('label' => 'Название Категории'))
            ->add('category_id' , ChoiceType::class, array(
                    'choices' => array(
                        'SUB1Категория' => 1 ,
                        'SUB2егория' => 1,
                        'SUB3егория' => 1 ,
                    ),))
            ->add('content', textType::class, array('label' => 'введите данные'))
            ->add('created_at',DateTimeType::class, array('label' => 'Дата Редактирования'))
            ->add('updated_at',DateTimeType::class, array('label' => 'Дата Редактирования'))
            ->add('save',SubmitType::class)
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Article::class,
        ));
    }
}
