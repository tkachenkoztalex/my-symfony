<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
// ...

class Cat
{


    /**
    * @var string
    *
    * @ORM\Column(name="name", type="string")
    */
    private $name;

    /**
    * @ORM\OneToMany(targetEntity="BlogPost", mappedBy="category")
    */
    private $blogPosts;

    public function __construct()
    {
    $this->blogPosts = new ArrayCollection();
    }

    public function getBlogPosts()
    {
    return $this->blogPosts;
    }

}