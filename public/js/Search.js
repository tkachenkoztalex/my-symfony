jQuery(document).ready(function() {
    var searchRequest = null;
    $("#form").keyup(function() {
        var minlength = '';
        var value = $(this).val();
        var entitySelector = $("#result").html('');
        if (value.length >= minlength ) {
            if (searchRequest != null)
                searchRequest.abort();
            searchRequest = $.ajax({
                type: "POST",
                url: "/searchController",
                data: {
                    'q' : value
                },
                dataType: "text",
                success: function(msg){
                        var result = JSON.parse(msg);
                        $.each(result, function(key, arr){
                            $.each(arr, function(id ,arr){
                                    if(entitySelector.hasClass('result-table')){
                                        entitySelector.append('<tr>' +
                                            '<th scope="row">'+
                                            ''+
                                            '</th>' +
                                            '<td>' +
                                            ''+arr.title+'' +
                                            '</td>' +
                                            '<td><a href="/article/edit/' +
                                            ''+id+'' +
                                            '">Редактировать</a></td>' +
                                            '<td><a href="/article/destroy/' +
                                            ''+id+'' +
                                            '" >Удалить</a></td>' +
                                            '</<tr>'
                                        )

                                    }else {
                                        if (key == 'entities') {
                                            if (id != 'error') {
                                                entitySelector.append('<div class="col-12 col-sm-2 col-lg-3" style="margin:50px; float: left ;height: 100px; overflow: hidden; text-overflow: ellipsis; background: #5bc0de; border-radius: 10px;" >' +
                                                    ''+arr.title+'' +
                                                    '<p>'+arr.content+'</p>' +
                                                    '</div>');
                                            } else {
                                                entitySelector.append('<li class="errorLi">'+value+'</li>');
                                            }
                                        }
                                    }

                            });
                        });
                }
            });
        }else {
            alert('hello')
        }
    });
});