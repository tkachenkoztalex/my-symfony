// main.js

var $ = require('jquery');
// JS эквивалентно любому нормальному пакету "bootstrap"
// не надо устанавливать это в переменную, просо запросите его
require('bootstrap-sass');

// или вы можете включить конкретные части
// require('bootstrap-sass/javascripts/bootstrap/tooltip');
// require('bootstrap-sass/javascripts/bootstrap/popover');

$(document).ready(function() {
    $('[data-toggle="popover"]').popover();
});